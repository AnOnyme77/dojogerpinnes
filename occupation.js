var oOccupation = {
    "Lundi" : [{"Heure":"18h30 - 20h", "Description" : "Judo (pour tous)"}],
    "Mardi" : [{"Heure":"18h - 19h30", "Description" : "Judo - Katas & Anti-agression"},
               {"Heure":"19h30 - 21h", "Description":" Jujutsu - Defense (Adultes)"}
               ],
    "Mercredi":[{"Heure":"18h - 19h15", "Description":"Jujutsu - Enfants (7 ans - 13 ans)"},
                {"Heure":"19h30 - 21h", "Description":"Self-Défense multi styles & Jujutsu - Defense (Adultes)"}],
    "Jeudi":[{"Heure":"19h - 20h", "Description":"Karate Kick Jitsu & Goshin Jutsu Special Défense"},
             {"Heure":"20h - 21h", "Description":"Kumite-Jitsu & Special Défense Séniors 60+"}],
    "Vendredi":[{"Heure":"18h - 19h30", "Description":"Judo - 4 à 12 ans"},
                {"Heure":"19h30 - 21h", "Description":"Judo - 12ans et +"}]
};
occupation(oOccupation);