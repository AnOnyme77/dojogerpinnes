function chargement(page){
    var script= document.createElement("script");
    script.src=page+".js";
    script.type="text/javascript";
    document.body.appendChild(script);
    document.body.removeChild(script);
}

function callback(oJson){
    var corps = document.getElementById("corps");
    while(corps.firstChild){
        corps.removeChild(corps.firstChild);
    }
    
    var texteNode = document.createElement("p");
    var texte = document.createTextNode("Le club "+oJson["nom"]+" est un club de "+oJson["sport"]);
    texteNode.appendChild(texte);
    
    if (oJson["description"]) {
        var descNode = document.createElement("p");
        var desc = document.createTextNode(oJson["description"]);
        descNode.appendChild(desc);
    }
    
    
    var lienNode = document.createElement("a");
    lienNode.href=oJson["site"];
    lienNode.target="blank";
    var lienTexte=document.createTextNode("Visitez leur site Web");
    lienNode.appendChild(lienTexte);
    
    corps.appendChild(texteNode);
     if (oJson["description"]) corps.appendChild(descNode);
    corps.appendChild(lienNode);
}
function occupation(oJson){
    var corps = document.getElementById("corps");
    while(corps.firstChild){
        corps.removeChild(corps.firstChild);
    }
    
    var table = document.createElement("table");
    
    for(var i=0;i< Object.keys(oJson).length;i++){
        var elem = Object.keys(oJson)[i];
        table.appendChild(createEntete(elem));
        for( var j=0;j<oJson[elem].length;j++){
            table.appendChild(createLigne(oJson[elem][j]));
        }
        
    }
    corps.appendChild(table);
}
function createLigne(obj){
    var tr = document.createElement("tr");
    
    var tdJour = document.createElement("td");
    tr.appendChild(tdJour);
    
    var tdHeure=document.createElement("td");
    var texteHeure = document.createTextNode(obj["Heure"]);
    tdHeure.appendChild(texteHeure);
    tr.appendChild(tdHeure);
    
    var tdDesc=document.createElement("td");
    var texteDesc = document.createTextNode(obj["Description"]);
    tdDesc.appendChild(texteDesc);
    tr.appendChild(tdDesc);
    
    return(tr);
}
function createEntete(jour){
    var tr = document.createElement("tr");
    
    var tdJour = document.createElement("td");
    var texteJour = document.createTextNode(jour);
    tdJour.appendChild(texteJour);
    tr.appendChild(tdJour);
    
    var tdHeure=document.createElement("td");
    tr.appendChild(tdHeure);
    
    var tdDesc=document.createElement("td");
    tr.appendChild(tdDesc);
    return(tr);
}
function image(){
    var dojo = document.getElementById("dojo");
    dojo.style.display='block';
}